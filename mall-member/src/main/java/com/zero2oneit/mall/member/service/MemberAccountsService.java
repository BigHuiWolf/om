package com.zero2oneit.mall.member.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zero2oneit.mall.common.bean.member.MemberAccounts;
import com.zero2oneit.mall.common.utils.R;


import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author Atzel
 * @create 2020-07-22
 * @description
 */
public interface MemberAccountsService extends IService<MemberAccounts> {

    void updateAccountsById(MemberAccounts oldAccounts, MemberAccounts newAccounts, int type, String remark) throws Exception;

    int commissionEdit(BigDecimal commission, String memberId);

    BigDecimal commissionInquire(String userId);

    String queryOld(String memberId);

    R withdrawComm(String tyep, BigDecimal amount);

//    R distributionEdit(String memberId, BigDecimal memberJf, BigDecimal platformJf, BigDecimal businessJf, BigDecimal businessMoney, int type, String businessId, String orderNo, BigDecimal orderMoney, String orderId);

    R yzShareCommissionConfirm(List<Map<String, String>> list);

    R ygShareCommissionConfirm(List<Map<String, String>> list);

    void shareMoneyEdit(BigDecimal shareMoney, String memberId);

    String shareMoneyQuery(String memberId);

//    R ygShareCommissionCreate(List<GoodsItem> list);
//
//    R yzShareCommissionCreate(List<FnyzGoodsItem> list);

    BigDecimal shareCommissions(String userId);

    void sharingCommissionEdit(BigDecimal withdrawalPrice, String memberId);

    void shareMoney(Map<String, String> map);

    void yzShareCommissionRefund(Map map);

}

